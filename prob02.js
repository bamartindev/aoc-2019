const { readFileSplit } = require('./util/file.js');

const comp = (nums, v1, v2) => {
    const ADD = 1;
    const MULT = 2;
    const STEP = 4;

    let halt = false;
    let pointer = 0;

    nums[1] = v1;
    nums[2] = v2;

    while (!halt) {
        const opcode = nums[pointer];
        const p1 = nums[nums[pointer + 1]];
        const p2 = nums[nums[pointer + 2]];
        const output = nums[pointer + 3];

        switch (opcode) {
            case ADD:
                nums[output] = p1 + p2;
                break;
            case MULT:
                nums[output] = p1 * p2;
                break;
            default:
                halt = true;
                break;
        }

        pointer += STEP;
    }

    return nums[0];
};


const p2 = nums => {
    for (let i = 0; i < 100; i++) {
        for (let j = 0; j < 100; j++) {
            if (comp([...nums], i, j) === 19690720) {
                console.log(`p2: ${100 * i + j}`);
                return;
            }
        }
    }
};

const run = () => {
    const val = readFileSplit(',', 'day2.txt');
    const nums = val.map(v => v | 0);
    const nums2 = [...nums];
    console.log("Problem 2");
    console.log(`p1: ${comp(nums, 12, 2)}`);
    p2(nums2);
};

module.exports = run;