const { readFileSplit } = require('./util/file.js');

const vm = (prg, input) => {
    const OP_ADD = 1;
    const OP_MULT = 2;
    const OP_INPUT = 3;
    const OP_OUTPUT = 4;
    const OP_JUMP_TRUE = 5;
    const OP_JUMP_FALSE = 6;
    const OP_LESS_THAN = 7;
    const OP_EQUALS = 8;

    const POSITION_MODE = 0;

    const INSTRUCTION_LENGTH = {
        [OP_ADD]: 4,
        [OP_MULT]: 4,
        [OP_INPUT]: 2,
        [OP_OUTPUT]: 2,
        [OP_JUMP_TRUE]: 3,
        [OP_JUMP_FALSE]: 3,
        [OP_LESS_THAN]: 4,
        [OP_EQUALS]: 4
    };

    let halt = false;
    let pointer = 0;

    while (!halt) {
        const opcode = prg[pointer];
        const rev = opcode.toString().split("").reverse().map(n => n | 0);
        const instruction = rev[0];
        const step = INSTRUCTION_LENGTH[instruction];

        let p1Mode = rev[2] || POSITION_MODE;
        let p1 = p1Mode === POSITION_MODE ? prg[prg[pointer + 1]] : prg[pointer + 1];

        let p2Mode = rev[3] || POSITION_MODE;

        let p2 = p2Mode === POSITION_MODE ? prg[prg[pointer + 2]] : prg[pointer + 2];

        let output = prg[pointer + (step - 1)];


        switch (instruction) {
            case OP_ADD:
                prg[output] = p1 + p2;
                break;
            case OP_MULT:
                prg[output] = p1 * p2;
                break;
            case OP_INPUT:
                prg[output] = input;
                break;
            case OP_OUTPUT:
                if (p1 !== 0) console.log(p1);
                break;
            case OP_JUMP_TRUE:
                if (p1 !== 0) {
                    pointer = p2;
                    continue;
                }
                break;
            case OP_JUMP_FALSE:
                if (p1 === 0) {
                    pointer = p2;
                    continue;
                }
                break;
            case OP_LESS_THAN:
                prg[output] = p1 < p2 ? 1 : 0;
                break;
            case OP_EQUALS:
                prg[output] = p1 === p2 ? 1 : 0;
                break;
            default:
                halt = true;
                break;
        }
        pointer += step;
    }
};

const run = () => {
    const val = readFileSplit(',', 'day5.txt');
    const prg = val.map(v => v | 0);

    console.log("Problem 5");
    console.log('p1:');
    vm([...prg], 1);
    console.log('p2:');
    vm([...prg], 5);
};

module.exports = run;