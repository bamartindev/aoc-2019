const fs = require('fs');
const path = require('path');

const splitFile = (delim, fileStr) => fileStr.split(delim);

const readFileSplit = (delim, fileName) => {
    const content = fs.readFileSync(path.join('inputs', fileName), 'utf8');
    return splitFile(delim, content);
}

module.exports = {
    readFileSplit
};