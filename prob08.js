const { readFileSplit } = require('./util/file.js');

const p1 = nums => {
    const WIDTH = 25;
    const HEIGHT = 6;
    const STEP = WIDTH * HEIGHT;

    let start = 0;
    let end = STEP;

    let fewestZero = Infinity;
    let result = 0;

    while (end <= nums.length) {
        const layer = nums.slice(start, end);
        const count = layer.filter(d => d === 0).length;
        if (count < fewestZero) {
            fewestZero = count;
            const count1 = layer.filter(d => d === 1).length;
            const count2 = layer.filter(d => d === 2).length;
            result = count1 * count2;
        }

        start = end;
        end = end + STEP;
    }

    console.log(`p1: ${result}`);
}

const p2 = nums => {
    const WIDTH = 25;
    const HEIGHT = 6;
    const STEP = WIDTH * HEIGHT;

    let start = 0;
    let end = STEP;
    let layers = [];

    while (end <= nums.length) {
        const layer = nums.slice(start, end);
        layers.push(layer);
        start = end;
        end = end + STEP;
    }

    //build image
    let image = [];
    for (let i = 0; i < STEP; i++) {
        // If 2, check next layer until a 1 or 0 is present then save in position.
        let pixel = 2;
        let layer = 0;

        do {
            pixel = layers[layer][i];
            layer += 1;
        } while (pixel === 2);

        image[i] = pixel;
    }


    // logic to pretty print the message - can this be cleaned up?
    console.log('\np2:\n');
    let row = [];
    for (let i = 1; i <= STEP; i++) {
        row.push(image[i - 1]);
        if (i % WIDTH === 0) {
            console.log(row.join('').replace(/0/g, ' '));
            row = [];
        }
    }
    console.log(row.join('').replace(/0/g, ' '));
}

const run = () => {
    const nums = readFileSplit('', 'day8.txt').map(n => n | 0);
    console.log('Problem 8');

    p1(nums);
    p2(nums);
}

module.exports = run;