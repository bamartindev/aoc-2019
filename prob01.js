const { readFileSplit } = require('./util/file.js');

const p1 = nums => {
    const total = nums.reduce((acc, val) => {
        return acc + Math.floor(val / 3) - 2;
    }, 0);

    console.log(`p1: ${total}`);
};

const subtotal = num => {
    const next = Math.max(Math.floor(num / 3) - 2, 0);
    return next + (next > 0 ? subtotal(next) : 0);
};

const p2 = nums => {
    const total = nums.reduce((acc, val) => {
        return acc + subtotal(val);
    }, 0);

    console.log(`p2: ${total}`);
};

const run = () => {
    const val = readFileSplit('\r\n', 'day1.txt');

    const nums = val.map(v => v | 0);
    console.log("Problem 1");
    p1(nums);
    p2(nums);
};

module.exports = run;