const { readFileSplit } = require('./util/file.js');

const checksum = (tree, next, level = 0) => {
    let total = level;
    const children = tree[next];

    if (children) {
        children.forEach(child => {
            total += checksum(tree, child, level + 1);
        });
    }

    return total;
}

const findHopsToSanta = (tree, yP = [], sP = []) => {
    if (yP.length === 0 && sP.length === 0) {
        Object.keys(tree).forEach(k => {
            if (tree[k] && tree[k].includes('YOU')) yP.push(k);
            if (tree[k] && tree[k].includes('SAN')) sP.push(k);
        });
    }

    let yN = yP[yP.length - 1];
    let sN = sP[sP.length - 1];


    Object.keys(tree).forEach(k => {
        if (tree[k] && tree[k].includes(yN)) yP.push(k);
        if (tree[k] && tree[k].includes(sN)) sP.push(k);
    });

    const intersection = yP.filter(v => -1 !== sP.indexOf(v));
    if (intersection.length > 0) {
        return yP.indexOf(intersection[0]) + sP.indexOf(intersection[0]);
    };

    return findHopsToSanta(tree, yP, sP);
}

const run = () => {
    const orbitMap = readFileSplit('\r\n', 'day6.txt');
    const tree = {};

    orbitMap.forEach(p => {
        const [orbit, orbiter] = p.split(')');
        if (tree[orbit] === undefined) {
            tree[orbit] = [];
        }
        tree[orbit].push(orbiter);
    });

    console.log('Problem 6');
    console.log(`p1: ${checksum(tree, "COM")}`);
    console.log(`p2: ${findHopsToSanta(tree)}`);
};

module.exports = run;