const { readFileSplit } = require('./util/file.js');

const vm = (prg, phase) => {
    let pointer = 0;
    let inputs = [phase];

    return input => {
        const OP_ADD = 1;
        const OP_MULT = 2;
        const OP_INPUT = 3;
        const OP_OUTPUT = 4;
        const OP_JUMP_TRUE = 5;
        const OP_JUMP_FALSE = 6;
        const OP_LESS_THAN = 7;
        const OP_EQUALS = 8;

        const POSITION_MODE = 0;

        const INSTRUCTION_LENGTH = {
            [OP_ADD]: 4,
            [OP_MULT]: 4,
            [OP_INPUT]: 2,
            [OP_OUTPUT]: 2,
            [OP_JUMP_TRUE]: 3,
            [OP_JUMP_FALSE]: 3,
            [OP_LESS_THAN]: 4,
            [OP_EQUALS]: 4
        };

        inputs.push(input);

        while (true) {
            const opcode = prg[pointer];

            const rev = opcode.toString().split("").reverse().map(n => n | 0);
            const instruction = rev[0];
            const step = INSTRUCTION_LENGTH[instruction];

            let p1Mode = rev[2] || POSITION_MODE;
            let p1 = p1Mode === POSITION_MODE ? prg[prg[pointer + 1]] : prg[pointer + 1];

            let p2Mode = rev[3] || POSITION_MODE;
            let p2 = p2Mode === POSITION_MODE ? prg[prg[pointer + 2]] : prg[pointer + 2];

            let output = prg[pointer + (step - 1)];

            switch (instruction) {
                case OP_ADD:
                    prg[output] = p1 + p2;
                    break;
                case OP_MULT:
                    prg[output] = p1 * p2;
                    break;
                case OP_INPUT:
                    const input = inputs.shift();
                    prg[output] = input;
                    break;
                case OP_OUTPUT:
                    pointer += step;
                    return p1;
                case OP_JUMP_TRUE:
                    if (p1 !== 0) {
                        pointer = p2;
                        continue;
                    }
                    break;
                case OP_JUMP_FALSE:
                    if (p1 === 0) {
                        pointer = p2;
                        continue;
                    }
                    break;
                case OP_LESS_THAN:
                    prg[output] = p1 < p2 ? 1 : 0;
                    break;
                case OP_EQUALS:
                    prg[output] = p1 === p2 ? 1 : 0;
                    break;
                default:
                    pointer += 1;
                    return null;
            }

            pointer += step;
        }
    }
};

const permutator = input => {
    const result = [];

    const permute = (arr, m = []) => {
        if (arr.length === 0) {
            result.push(m);
        } else {
            arr.forEach((_, i) => {
                const cur = arr.slice();
                const next = cur.splice(i, 1);
                permute(cur.slice(), m.concat(next));
            });
        }
    }

    permute(input);
    return result;
}

const run = () => {
    const val = readFileSplit(',', 'day7.txt');
    let prg = val.map(v => v | 0);

    console.log("Problem 7");

    let arr = [0, 1, 2, 3, 4];
    let permutations = permutator(arr);

    let max = -Infinity;
    permutations.forEach(perm => {
        let output = 0;
        perm.forEach(input => {
            output = vm([...prg], input)(output);
            if (output > max) max = output;
        });
    });
    console.log(`p1: ${max}`);

    arr = [5, 6, 7, 8, 9];
    permutations = permutator(arr);

    let amplifiers = [];
    let signals = [];
    permutations.forEach(perm => {
        let output = 0;
        let i = 0;
        let last = 0;

        amplifiers = [
            vm([...prg], perm[0]),
            vm([...prg], perm[1]),
            vm([...prg], perm[2]),
            vm([...prg], perm[3]),
            vm([...prg], perm[4])
        ];

        while (true) {
            output = amplifiers[i](last);
            if (output === null && i === 4) {
                break;
            }
            if (output !== null) {
                last = output;
            }
            i = (i + 1) % perm.length;
        }
        signals.push(last);
    });

    let result = signals.sort((a, b) => b - a)[0];
    console.log(`p2: ${result}`);
};

module.exports = run;